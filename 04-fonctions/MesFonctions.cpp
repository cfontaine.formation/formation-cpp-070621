#include "MesFonctions.h"
#include <iostream>

// Fichier .cpp contient les D�finitions des fonctions

using namespace std;

int somme(int v1, int v2)
{
	return v1 + v2;
}

void afficher(int a)
{
	cout << a << endl;
	//return;
}

double maximum(double v1, double v2)
{
	return v1 > v2 ? v1 : v2;
}

bool even(int valeur)
{
	return valeur%2==0;
}


void testParamValeur(int a)
{
	cout << a << endl;
	a = 34;
	cout << a << endl;
}

// Les param�tres par d�faut sont uniquement d�fini dans la d�claration de la fonction (.h)
void testParamDefaut(bool b, double d, int i)
{
	cout << b << " "<< d << " "<< i << endl;
}

// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* ptr)
{
	cout << *ptr << endl;
	*ptr = 42;
	cout << *ptr << endl;
}


// Exercice Tableau
// - �crire un m�thode qui affiche un tableau d�entier
// - �crire une m�thode qui permet de saisir :
//		- La taille du tableau
//		- Les �l�ments du tableau
// - �crire une m�thode qui calcule :
//		- le maximum
//		- la moyenne
void afficherTab(int* ptrTab, int size)
{
	cout << "[ ";
	for (int i = 0; i < size; i++)
	{
		cout << ptrTab[i] << " ";
	}
	cout << "]";
}

int* saisirTab(int& size)
{
	cin >> size;
	int* td = new int[size];
	for (int i = 0; i < size; i++)
	{
		cout << "t[" << i << "]=";
		cin >> td[i];
	}

	return td;
}

void calculTab(int* tab, int size, int& max, double& moy)
{
	max = tab[0];
	double somme = tab[0];
	for (int i = 1; i < size; i++)
	{
		if (tab[i] > max)
		{
			max = tab[i];
		}
		somme += tab[i];
	}
	moy = somme / size;
}

// Passage de param�tre par r�f�rence
// Comme avec le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int& ref)
{
	cout << ref<<endl;
	ref = 42;
	cout << ref << endl;
}

void testParamReferenceCst(const int& ref)
{
	cout << ref << endl;
	//ref = 4;
}

int* testRetour()
{
//	int z = 12;	// variable locale
	int* z = new int;
	*z = 12;
	return z;
}

bool estPresent(char* ptrT, int size, char c)
{
	for(int i=0;i<size;i++)
		if (ptrT[i] == c)
		{
			return true;
		}
	return false;
}

// Surcharge de fonction
// Plusieurs fonctions peuvent avoir le m�me nom
// Ces arguments doivent �tre diff�rents en nombre ou/et en type pour qu'il n'y ai pas d'ambiguit� pour le compilateur
int multiplier(int a, int b)
{
	cout << "2 entiers" << endl;
	return a*b;
}

double multiplier(double d1, double d2)
{
	cout << "2 doubles" << endl;
	return d1*d2;
}

double multiplier(int a, double b)
{
	cout << "1 entier et  un double" << endl;
	return a*b;
}

int multiplier(int d, int e, int a)
{
	cout << "3 entiers" << endl;
	return d*e*a;
}

int factorial(int n) // factoriel= 1* 2* � n
{
	if (n <= 1)// condition de sortie
	{
		return 1;
	}
	else
	{
		return factorial(n - 1) * n;
	}
}

// Classe de m�morisation
void testClassMem()
{
	// Entre 2 ex�cutions inc conserve sa valeur
	static int inc=100;	// par d�faut initialis� � 0
	cout << inc << endl;
	
	// extern
	cout<<PI << endl;//varGlobale2<< endl;
	inc++;
}
