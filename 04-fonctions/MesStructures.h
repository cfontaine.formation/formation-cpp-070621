#pragma once
#include<string>

struct Contact
{
	// Champs
	std::string prenom;
	std::string nom;
	mutable int age;	// mutable => si la structure est constante,
						// on pourra quand m�me modifier les champs mutable


	// M�thode
	void afficher();	// En c++ une structure peut contenir des fonctions
};
