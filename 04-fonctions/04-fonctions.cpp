#include <iostream>
#include <string>
#include "MesFonctions.h"
#include "MesStructures.h"

using namespace std;

// D�claration de la fonction => D�placer dans MesFonctions.h
//int somme(int, int);
//void afficher(int b);

const double PI = 3.14;
static int varGlobale2=12;	 // avec static la variable n'est visible que dans ce fichier


// en C++98
enum Direction { NORD=90, EST=0, OUEST=180, SUD=270 };


// en c++11
enum class Motorisation {
	ESSENCE,
	DIESEL,
	ELECTRIQUE,
	GPL
};

int main(int argc,char* argv[])
{
/*	//static int inc2;
	// Appel de methode
	cout << somme(2, 5) << endl;
	somme(2, 5);

	// Appel de methode (sans retour)
	afficher(somme(2, 5));

	// Exercice Fonction maximum
	cout << "Entrer 2 nombres" << endl;
	double a, b;
	cin >> a >> b;
	double max = maximum(a, b);
	cout << "maximum= " << max << endl;

	// Param�tre pass� par valeur
	// C'est une copie de la valeur du param�tre qui est transmise � la m�thode
	int v = 10;
	testParamValeur(v);
	cout << "v=" << v << endl;

	// Exercice Fonction parit�
	cout << "Entrer un nombre entier" << endl;
	cout << even(2) << endl;
	cout << even(3) << endl;

	// Param�tre par d�faut
	testParamDefaut(true, 3.4, 42);
	testParamDefaut(true, 4.5);
	testParamDefaut(false);

	// Param�tre pass� par adresse
	// La valeur de la variable pass�e en param�tre est modifi�e, si elle est modifi�e dans la m�thode
	int v3 = 3;
	testParamAdresse(&v3);	// on passe en param�tre l'adresse de la variable v3
	cout << v3 << endl;

	// Les tableaux ne peuvent �tre passer que par adresse
	int t[] = { 1,4,6,4 };
	afficherTab(t, 4);	// le nom du tableau correspond � un pointeur sur le premier �l�ment du tableau

	// Param�tre pass� par r�f�rence
	testParamReference(v3);
	testParamReferenceCst(v3);
	cout << v3 << endl;

	// avec une r�f�rence constante, on peut aussi passer des lit�raux
	testParamReferenceCst();
	testParamReferenceCst(45);

	char chr[] = {'a','z','e','r'};
	cout<<estPresent(chr, 4, 'z')<<endl;
	cout << estPresent(chr, 4, 'y') << endl;

	// Surcharge de fonction
	cout << multiplier(2, 2) << endl;
	cout << multiplier(2.5, 6.2) << endl;
	cout << multiplier(2, 4.5) << endl;
	cout << multiplier(1, 2, 4) << endl;
	cout << multiplier('\02', '\04') << endl;	// Convertion des caract�res en entier
	cout << multiplier(5.6, 4.0F) << endl;		// Convertion du float en double
	// cout << multiplier(5.3,2) << endl;		// Erreur,si il n' y a pas de fonction multiplication avec pour param�tre un double et un entier

	// Fonction recursive
	cout << factorial(3) << endl;

	// Param�tre de la fonction main
	for (int i = 0; i < argc; i++) {
		cout << argv[i] << endl;
	}
	
	// /!\ une fonction ne doit pas retourner une r�f�rence ou un pointeur sur une variable locale � une fonction
	int* ts = testRetour();
	cout << *ts << endl;
	delete ts;

	int size;
	int* tr = saisirTab(size);
	afficherTab(tr, size);
	double moyenne;
	int maximum;
	calculTab(tr, size, maximum, moyenne);
	cout << "\nmaximum=" << maximum << " moyenne=" << moyenne << endl;

	// Classe de m�morisation
	// Static
	testClassMemStatic();
	testClassMemStatic();
	testClassMemStatic();
	testClassMemStatic();
	testClassMemStatic();
	//cout << inc;
	Contact c1;
	c1.prenom = "John";
	c1.nom = "Doe";
	c1.age= 58;
	cout << c1.prenom << " " << c1.nom << " " << c1.age << endl;

	Contact c2 = c1;
	cout << c2.prenom << " " << c2.nom << " " << c2.age << endl;

	Contact c3 = { "Jane","Doe",34 };
	cout << c3.prenom << " " << c3.nom << " " << c3.age << endl;

	Contact* ptrC = new Contact;
	(*ptrC).prenom = "alan";
	ptrC->nom = "Smithee";
	ptrC->age = 45;
	cout << ptrC->prenom << " " << ptrC->nom << " " << ptrC->age << endl;
	

	if (c1.prenom == c3.prenom && c1.nom == c3.nom && c1.age == c3.age) {
		cout << "c1 et c3 sont �gaux" << endl;
	}

	c1.afficher();
	c2.afficher();
	ptrC->afficher();


	delete ptrC;
	const Contact cCst= { "Jane","Doe",34 };
	cout << cCst.prenom << endl;
	//cCst.prenom = "azerty";
	cCst.age = 36;*/

	// chaine de caract�res C
	const char* strC = "Bonjour";
	cout << strC << endl;
	cout << strC[2] << endl;
	//strC[2] = 'A';
	cout << sizeof(strC) << endl;

	char strC2[] = "hello World";
	strC2[0] = 'H';
	cout << strC2 << endl;

	// en C++
	string str = "Hello";
	cout << str << endl;
	string str1 = string("Bonjour");
	string str2 = string(7,'a');
	cout << str1 << " " << str2 << endl;

	// Concat�nation

	str += " World";
	cout << str << endl;
	string str3 = " !";
	str = str + str3;
	cout << str << endl;
	str.append("!!!!");
	cout << str << endl;
	str.push_back('?');
	cout << str << endl;

	// Comparaison 
	if (str1 == str2) 
	{
		cout << "==" << endl;
	}
	else
	{
		cout << "!=" << endl;
	}

	if (str2 < str1)
	{
		cout << "Inf�rieur" << endl;
	}
	else
	{
		cout << "Sup�rieur" << endl;
	}

	cout << str.length() << " " << str.size() << endl;
	cout << str.empty() << endl;
	//Acc�s � un caract�re de la chaine
	cout << str[2] << endl;
	cout << str.at(2) << endl;

	//cout << str[40] << endl; // => erreur
	//cout << str.at(40) << endl; // exception

	str[0] = 'h';
	str.at(0) = 'H';

	// Modification
	cout << str.substr(5)<<endl;
	cout << str.substr(6, 5) << endl;

	str.insert(5, "----");
	cout << str << endl;
	str.erase(5, 4);
	cout << str << endl;
	str.erase(11);
	cout << str << endl;

	cout << str.find("World") << endl;
	cout << str.find('o') << endl;
	cout << str.find('o', 5) << endl;
	cout << str.find('o', 8) <<" "<<string::npos << endl;

	const char* strCnv = str.c_str();

	for (string::iterator it = str.begin(); it != str.end(); it++)
	{
		cout << *it << endl;
	}

	for (string::reverse_iterator it = str.rbegin(); it != str.rend(); it++)
	{
		cout << *it << endl;
	}

	// En C++11
	for (auto c : str)
	{
		cout << c << endl;
	}

	str.clear();
	cout << str << endl;
	cout << str.empty() << endl;

	Direction dir = Direction::EST;
	int valeurDir = dir;

	cout << valeurDir << " " << dir << endl;

	if (dir == Direction::EST) {
		cout << "Est" << endl;
	}

	int vdir = 270;
	switch (vdir) {
	case 0:
		dir = Direction::EST;
		break;
	case 90:
		dir = Direction::NORD;
		break;
	case 180:
		dir = Direction::OUEST;
		break;
	case 270:
		dir = Direction::SUD;
		break;
		//default::
		// g�nrer une exception
	}

	Motorisation mt = Motorisation::ELECTRIQUE;
	int valeurMt =static_cast<int>( mt);
	cout << valeurMt << endl;

	switch (mt) {

	case Motorisation::ESSENCE:
		cout << "Essence" << endl;
		break;
	case Motorisation::DIESEL:
			cout << "Diesel" << endl;
			break;
	}
	return 0;
}



// D�finition de la fonction => D�placer dans MesFonctions.cpp
//int somme(int v1, int v2)
//{
//	return v1 + v2;
//}

//void afficher(int a)
//{
//	cout << a << endl;
//	//return;
//}

