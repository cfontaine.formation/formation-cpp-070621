// Fichier .h => contient la d�claration des fonctions

#pragma once				// Indique au compilateur de n'int�grer le fichier d�en-t�te qu�une seule fois, lors de la compilation d�un fichier de code source
//#ifndef MES_FONCTIONS_H	// idem #pragma once
//#define MES_FONCTIONS_H

// d�claration d'une variable qui a �t� d�finie dans 04-fonctions.cpp. Il n' y a pas d'allocation m�moire
extern const double PI;
//extern int varGlobale2;	//varGlobale2 est static dans 04-fonctions.cpp, on ne peut pas l'utiliser que dans ce fichier

// D�claration d'une fonction
int somme(int, int);

// D�claration d'une fonction sans retour => void
void afficher(int b);

// Exercice maximum
double maximum(double v1, double v2);

// Exercice parit�
bool even(int valeur);

// Passage de param�tre par valeur
void testParamValeur(int);

// On peut d�finir des valeurs par d�faut pour les param�tres.
// Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par d�faut dans la d�claration des fonctions
void testParamDefaut(bool b, double d=3.0, int i=0);

// Passage de param�tre par adresse
// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* ptr);

// Passage d'un tableau, comme param�tre d'une fonction
// On ne peut pas passer un tableau par valeur uniquement par adresse
void afficherTab(int* ptrTab, int size);
//void afficherTab(int ptrTab[], int size);	 pour le passage de tableau en param�tre on peut aussi utiliser cette syntaxe

// Exercices m�thodes: tableau
int* saisirTab(int& size);
void calculTab(int* tab, int size, int& max, double& moy);

// Passage de param�tre par r�f�rence
// Comme avec  le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int& ref);
void testParamReferenceCst(const int &ref=10);


// ERREUR => fonctions qui retournent une r�f�rence ou un pointeur sur une variable locale
int* testRetour();

// D�claration d'un  fonction inline, on place le code de la fonction dans le fichier.h
// inline => permet de d�finir des fonctions qui seront directement �valu�es � la compilatio
double tripler(double d) // inline
{
	return d * 3.0;
}

// Exercice pr�sence de caract�re
bool estPresent(char* ptrT, int size, char c);

// Surcharge de la fonction multiplication
int multiplier(int a, int b);
double multiplier(double d1, double d2);
double multiplier(int a, double b);
int multiplier(int d, int e, int a);

// R�cursivit�
int factorial(int n);

// Test de classe de m�morisation
void testClassMem();
//#endif // fin ifndef idem #pragma once