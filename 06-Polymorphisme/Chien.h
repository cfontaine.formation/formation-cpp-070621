#pragma once
#include "Animal.h"
#include <string>

class Chien :   public Animal
{
	std::string nom;

public:
	 
	Chien(int poid, int age, std::string nom) : Animal(poid, age), nom(nom)
	{

	}

	std::string getNom() const
	{
		return nom;
	}

	void emettreSon();
};

