#include <iostream>
#include "Animal.h"
#include "Chien.h"
#include "Chat.h"
#include "Refuge.h"

#include <iostream>
using namespace std;

int main()
{
	/*Animal an1(3000, 4);
	an1.emettreSon();*/

	Chien* ch1 = new Chien(4000, 3, "Laika");
	ch1->emettreSon();
	
	// Polymorphisme
	Animal* an2 = new Chien(3150, 7, "Tom");
	an2->emettreSon();

	Animal* an3 = new Chat(3000, 5, 7);
	an3->emettreSon();

	//Chien* ch2 = (Chien*) an2;
	Chien* ch2 = dynamic_cast<Chien*>(an2);
	if (ch2 != 0) {
		cout << ch2->getNom() << endl;
	}
	Refuge r;
	r.ajouter(new Chien(4000, 3, "Laika"));
	r.ajouter(new Chat(2000, 5, 7));
	r.ajouter(new Chat(6000, 10, 4));
	r.ajouter(new Chien(3150, 7, "Tom"));
	r.ecouter();
}
