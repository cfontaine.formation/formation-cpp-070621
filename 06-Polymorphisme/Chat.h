#pragma once
#include "Animal.h"
class Chat : public Animal
{
	int nbVie;

public:
	Chat(int poid, int age, int nbVie) : Animal(poid, age), nbVie(nbVie)
	{

	}

	int getNbVie()const {
		return nbVie;
	}

	void emettreSon();
};

