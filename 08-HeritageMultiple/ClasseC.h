#pragma once
#include "ClasseA.h"
#include"ClasseB.h"
class ClasseC : public ClasseA, public ClasseB 
{
	int c;
public:
	ClasseC(int a, int b, int c) : ClasseA(a), ClasseB(b), c(c)
	{

	}

	int fc();
	void calcul();
};

