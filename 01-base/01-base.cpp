#include <iostream>	// inclure le fichier d'en-tête iostream qui définit les objets de flux d'entrée/sortie standard
#include <bitset>

using namespace std;// placer tous les éléments de l'espace de nom std dans la portée
/*
	Un commentaire

	sur plusieurs lignes

*/

// commentaire sur une ligne

// Variable globale
int varGlobale = 3456;

// Fonction main => point d'entré du programme
int main()
{
	// std::cout << "Hello World!"<<std::endl;	// cout => écrire dans le flux de sortie (console),  endl => retour à la ligne

	// Déclarer une variable
	int i;

	// Initialiser une variable
	i = 42;

	// déclaration et initialisation
	double d = 12.3;

	cout << i << " " << d << endl;

	// Déclaration  multiple
	int mb, mc = 12;
	mb = 23;

	// Déclarer et initialiser une variable
	int j = 10;			// en C
	int jcpp98(10);		// en C++
	int jcpp11{ 10 };		// en C++11

	// Littéral entier => par défaut de type int
	long l = 12345L;		 // litéral long => L
	unsigned int u = 11U;	 // litéral unsigned => U
	long long lli = 100LL;	 // litéral long long => LL en C++ 11

	// Changement de base
	int decimal = 42;	// base par défaut: décimal
	int hexa = 0xFF23;	// héxadécimal => 0x
	int octal = 0321;	// octal => 0
	int bin = 0b100101; // binaire => 0b en C++14

	// Avec cout
	// Manipulateur hex => affiche les valeurs numérique qui suivent en héxadécimal
	// Manipulateur oct => affiche les valeurs numérique qqui suivent en octal
	// Manipulateur dec => affiche les valeurs numérique qui suivent en décimal
	// Il n'y a de manipulateur pour les nombres binaires on peut utiliser un objet bitset
	cout << decimal << " " << hex << hexa << " " << oct << octal << " " << bitset<32>(bin) << dec << endl;

	// Littéral virgule flottante
	double d1 = 1.5;
	double d2 = 5.;
	double d3 = .5;
	double d4 = 1.32e3;

	// Littéral virgule flottante =>double
	float f = 1.4F;			// littéral float => F
	long double ld = 1234L;	// litéral long double => L
	cout << f << " " << ld << endl;

	// Littéral booléen
	bool b = true; // ou false

	// Littéral caractère
	char c = 'a';
	char cHexa = '\x41';	// caractère en hexadécimal
	char cOctal = '\32';	// caractère en octal
	cout << c << " " << cHexa << " " << cOctal << endl;

	// Typage implicite c++11
	// Le type de la variable est définie à partir de l'expression d'initialisation
	auto implicite = 23;
	auto implicite2 = 4.5F;

	// Attention aux chaine de caratères : avec auto le type sera const char* et pas std::string
	auto impliciteStr = "hello";

	// decltype  C++ 11 = > déclarer qu'une variable est de même type qu'une autre
	decltype(c)  implicite3;

	// Constante
	const double PI = 3.14;
	//  PI=2;               // Erreur: on ne peut pas modifier une constante

	// Variable globale
	int it = 45;
	if (it == 45)
	{
		int somme = it + 34;
	}
	// cout << somme << endl;

	cout << varGlobale << endl;
	// On déclare une variable locale qui a le même le nom que la variable globale
	int varGlobale = 2;
	// La variable globale est masquée par la variable locale
	cout << varGlobale << endl;
	// Pour accéder à la variable globale, on utilise l’opérateur de résolution de portée ::
	cout << ::varGlobale << endl;

	// Opérateur
	// Opérateur arithmétique
	int aa = 2;
	int bb = 5;
	int res = aa + bb;
	cout << res << " " << res % 4 << endl;	// % => modulo (reste de division entière) uniquement avec des entiers positif

	// Pré-incrémentation
	int inc = 0;
	res = ++inc;	// inc=1	res=1
	cout << inc << " " << res << endl;

	// Post-incrémentation
	inc = 0;
	res = inc++;	// res=0 inc=1
	cout << inc << " " << res << endl;

	// Affection composée
	res += 10;	//  correspond à res=res+10;
	res *= 2;	//  correspond à res=res*2,
	res &= 3;	//  correspond à res =res & 3;

	// Opérateur de comparaison
	int val;
	cin >> val;
	bool test = val > 100;	// Une comparaison a pour résultat un booléen
	cout << test << endl;

	// Opérateur logiques
	// non
	cout << !test << endl; // non

	// Opération court-circuit
	// && et || (évaluation garantie de gauche à droite)
	// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	// || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
	int o = 10;
	int p = 30;
	bool test2 = o > 30 && p < 50;	// comme o > 30 est faux,  p<50 n'est pas évalué
	bool test3 = o < 30 || p>50;

	// Opérateur binaire (uniquement avec des entiers)

	// Pour les opérations sur des binaires, on utilise les entiers non signé
	unsigned int b1 = 0b101011;

	cout << ~b1 << " " << bitset<32>(~b1) << endl;					// Complémént: 1 -> 0 et 0 -> 1
	cout << (b1 & 0b10) << " " << bitset<32>(b1 & 0b10) << endl;	// Et bit à bit	 2 10
	cout << (b1 | 0b100) << " " << bitset<32>(b1 | 0b100) << endl;	// Ou bit à bit 47 101111
	cout << (b1 ^ 0b110) << " " << bitset<32>(b1 ^ 0b110) << endl;	// Ou exclusif bit à bit 45 101101

	cout << (b1 >> 1) << endl;	// Décalage à droite de 1 10101
	cout << (b1 >> 2) << endl;	// Décalage à droite de 2 1010
	cout << (b1 << 1) << endl;	// Décalage à gauche de 1 1010110
	cout << (b1 << 3) << endl;  // Décalage à gauche de 2 101011000

	// Opérateur sizeof
	cout << sizeof(b1) << endl;	// nombre d'octets de la variable b1 => 4
	cout << sizeof(long double) << endl; // nombre d'octets du type long double => 8

	// Opérateur séquentiel , toujours évalué de gauche -> droite
	inc = 0;
	res = (inc++, o + p); // inc++ est exécuté et l'expression retourne o+p
	cout << inc << " " << res << endl; // inc = 1 res = 3

	// Conversion implicite => rang inférieur vers un rang supérieur (pas de perte de donnée)
	int convI = 12;
	double convD = convI; // convD=12.0

	double convRes = convD + convI;

	// Promotion numérique => short, bool ou char dans une expression => convertie automatiquement en int
	short s1 = 1;
	short s2 = 2;
	int pnRes = s1 + s2;

	// entier convertion en boolean
	bool convB1 = true;
	convI = convB1; //true->1
	bool convB2 = false;
	convI = convB2; // false -> 0

	// boolean convertion en entier
	convB1 = 50; //=> true !=0
	convB2 = 0; // => false ==0

	// convertion explicite
	int te = 11;
	int div = 2;
	int res2 = te / div; // 5

	double resD = ((double)te) / div; // 5.5  en C
	resD = static_cast<double>(te) / div; // 5.5 en C++ -> le compilateur fait plus de vérification (sur le type)
	cout << res2 << " " << resD << endl;

	// Opérateur d'affectation conversion systémathique (implicite et explicite)

	double affD = 36.8;
	int affI = affD; // 36

	return 0;	 // Si le code, c'est exécuté normalement, la fonction main doit retourner 0 
}
