#include "Point.h"

std::ostream& operator<<(std::ostream& os, Point& p)
{
    return os << "(" << p.x << "," << p.y << ")";
}
