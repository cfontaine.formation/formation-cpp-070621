#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <stack>
#include<algorithm>
#include "Point.h"

using namespace std;

void afficher(int val) {
    cout << val << endl;
}

bool impaire(int val)
{
    return val % 2 != 0;
}


int main()
{
    vector<Point> v;
    v.push_back(Point(2,1));
    v.push_back(Point(5,4));
    v.push_back(Point(6, 1));
    cout << v.size() << endl;
    cout << v[0].getX() << endl;

    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i].getX() << endl;
    }

    for (auto p : v) // C++11 
    {
        cout << p.getX() << endl;
    }

    for (auto it = v.begin(); it != v.end(); it++)
    {
        cout << (*it).getX() << endl;
    }
    v.erase(v.begin() + 1, v.end());

    cout << "---" << endl;
    for (auto p : v) // C++11 
    {
        cout << p.getX() << endl;
    }

    //Liste
    list<Point> lstPoint;
    lstPoint.push_back(Point(2, 1));
    lstPoint.push_back(Point(5, 4));
    lstPoint.push_back(Point(6, 1));
    for (auto it = lstPoint.begin(); it != lstPoint.end(); it++)
    {
        cout << (*it).getX() << endl;
    }

    map<int, string> m;
    m[45] = "Hello";
    m[12] = "World";
    m[5] = "Bonjour";

    cout << m[5] << endl;
    for (auto it = m.begin(); it != m.end(); it++)
    {
        cout << it->first << " <-> " << it->second << endl;
    }
    m.erase(12);
    m.clear();
    cout << m.empty() << endl;

    // stack
    stack<int> pile;
    pile.push(3);
    pile.push(4);
    pile.push(67);
    cout << pile.top() << endl;
    pile.pop();
    cout << pile.top() << endl;
    pile.pop();
    pile.pop();
    cout << pile.empty() << endl;

    // algorithme
    vector<int>v2(10);
    fill(v2.begin() + 1, v2.end() - 1, 2);
    v2[0] = 7;
    v2[9] = 3;
    cout << "min=" << *(min_element(v2.begin(), v2.end())) << endl;

    for_each(v2.begin(), v2.end(), afficher);
    cout << "--" << endl;
    auto it1=find_if(v2.begin(), v2.end(),impaire);
    cout << *it1 << endl;
    remove_if(v2.begin(), v2.end(), impaire);
    for_each(v2.begin(), v2.end(), afficher);


    Point pA(3, 4);
    cout << pA << endl;
}

