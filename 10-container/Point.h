#pragma once
#include<ostream>
class Point
{
	int x;
	int y;
public:
	Point(int x, int y) : x(x), y(y)
	{

	}

	int getX()const
	{
		return x;
	}
	int getY()const
	{
		return y;
	}
	friend std::ostream& operator<<(std::ostream& os, Point& p);
};

