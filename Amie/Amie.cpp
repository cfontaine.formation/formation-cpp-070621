
#include <iostream>
#include "Test.h"

void fonctionAmie(Test& t) {
    t.valeur = 5;
}

int main()
{
    Test t1(120);
    t1.afficher();
    fonctionAmie(t1);
    t1.afficher();
}


