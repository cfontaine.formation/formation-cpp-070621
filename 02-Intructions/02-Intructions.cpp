#include <iostream>
using namespace std;

int main()
{
	// Condition if
	int v;
	cin >> v;
	if (v > 100)
	{
		cout << "v>100" << endl;
	}
	else if (v < 100)
	{
		cout << "v<100" << endl;
	}
	else
	{
		cout << "v==100" << endl;
	}

	int a, b;
	cin >> a >> b;
	int c = a + b;
	cout << a << " + " << b << " = " << c << endl;

	// Exercice: Moyenne
	// Saisir 2 nombres entiers et afficher la moyenne dans la console
	int a1, a2;
	cin >> a1 >> a2;
	double d = (a1 + a2) / 2.0;
	cout << "moyenne=" << d;

	// Trie de 2 Valeurs
	// Saisir 2 nombres à virgule flottante et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
	double d1, d2;
	cin >> d1 >> d2;
	if (d1 > d2)
	{
		cout << d2 << " < " << d1 << endl;
	}
	else
	{
		cout << d1 << " < " << d2 << endl;
	}

	// Exercice : Intervalle
	// Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
	int vi;
	cin >> vi;
	if (vi > -4 && vi <= 7) {
		cout << vi << " fait parti de l'intervalle" << endl;
	}

	// Condition: switch
	int jours;
	cin >> jours;
	switch (jours)
	{
	case 1:
		std::cout << "Lundi" << std::endl;
		break;
	case 6:
	case 7:
		std::cout << "Week end !" << std::endl;
		break;
	default:
		std::cout << "Un autre jour" << std::endl;
		break;
	}

	// Exercice : Calculatrice
	// Faire un programme calculatrice
	//	Saisir dans la console
	//	- un double
	//	- une caractère opérateur qui a pour valeur valide : + - * /
	//	- un double

	// Afficher:
	// - Le résultat de l’opération
	// - Une message d’erreur si l’opérateur est incorrecte
	// - Une message d’erreur si l’on fait une division par 0
	double v1, v2;
	char op;
	cin >> v1 >> op >> v2;
	switch (op)
	{
	case '+':
		cout << " = " << v1 + v2 << endl;
		break;
	case '-':
		cout << " = " << v1 - v2 << endl;
		break;
	case '*':
		cout << " = " << v1 * v2 << endl;
		break;
	case '/':
		if (v2 == 0)
		{
			cout << "Division par 0";
		}
		else
		{
			cout << " = " << v1 / v2 << endl;
		}
		break;
	default:
		cout << op << " l'opérateur est inconnue" << endl;
	}

	// Condition: opérateur ternaire
	// Valeur absolue
	// Saisir un nombre et afficher la valeur absolue sous la forme | -1.85 | = 1.85
	int abs;
	cin >> abs;
	int r = abs > 0 ? abs : -abs;
	cout << "|" << abs << "| = " << r << endl;

	// Boucle: while
	int k = 0;
	while (k < 10)
	{
		cout << k << endl;
		k++;
	}
	cout << "----" << endl;

	// Boucle: do  while
	do {
		cout << k << endl;
		k++;
	} while (k < 20);

	// Boucle: for
	for (int i = 10; i > 0; i--)
	{
		cout << "i=" << i << endl;
	}

	// Exercice : Table de multiplication
	// Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	//	1 X 4 = 4
	//	2 X 4 = 8
	//	…
	//	9 x 4 = 36
	int vm;
	cin >> vm;
	if (vm > 1 && vm < 9)
	{
		for (int i = 1; i < 10; i++)
		{
			cout << i << " x " << v << "=" << i * vm << endl;
		}
	}
	else
	{
		cout << "En dehors de l'interval 1 à 9" << endl;
	}


	// Exercice : Quadrillage
	// Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
	//	ex : pour 2 3
	//	[] []
	//	[] []
	//	[] []
	int col, row;
	cout << "Entrer le nombre de colonne" << endl;
	cin >> col;
	cout << "Entrer le nombre de ligne" << endl;
	cin >> row;
	for (int r = 0; r < row; r++) {
		for (int c = 0; c < col; c++) {
			cout << "[ ]";
		}
		cout << endl;
	}

	// Instructions de branchement
	// break
	for (int i = 0; i < 10; i++)
	{
		cout << "i=" << i << endl;
		if (i == 3)
		{
			break;	// break => termine la boucle
		}
	}

	// continue
	for (int i = 0; i < 10; i++)
	{
		if (i == 3)
		{
			continue;	// continue => on passe à l'itération suivante
		}
		cout << "i=" << i << endl;
	}

	// goto
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			cout << "i=" << i << "j=" << j << endl;
			if (i == 3)
			{
				goto EXIT_LOOP;	// Utilisation de goto pour sortir de 2 boucles imbriquée
			}
		}

	}
EXIT_LOOP:	for (;;) // while(true)	  => boucle infinie
{
	int v2;
	cin >> v2;
	if (v2 < 1 || v2 > 9)
	{
		break;
	}
	for (int i = 1; i < 10; i++)
	{
		cout << i << " x " << v2 << "=" << i * v2 << endl;
	}
}
}