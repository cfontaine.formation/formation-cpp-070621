#pragma once
#include<exception>
#include<string> 
class AgeNegatifException :   public std::exception
{
	std::string msg;

public:
	AgeNegatifException(int age) : msg("L'age est negatif : " + std::to_string(age))
	{
	}

	const char* what();
};

