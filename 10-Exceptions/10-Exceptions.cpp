#include <iostream>
#include <string>
#include "AgeNegatifException.h"

using namespace std;

void traitementAge(int age);
void etatCivil(int age);

int main()
{
	string str = "Hello";
	try {
		cout << "début du programme" << endl;
		int age;
		cin >> age;
		etatCivil(age);

		cout << str.at(100) << endl;
		cout << "Suite Programme" << endl;
	}
	catch (std::out_of_range e)
	{
		cout << "en dehors de la plage de la chaine" << endl;
	}
	catch (AgeNegatifException e)
	{
		cout << e.what() << endl;
	}
	catch (...) // toutes les autres exceptions
	{
		cout << "Traitement exception" << endl;
	}
	cout << "Fin Programme" << endl;
}


void etatCivil(int age) throw(AgeNegatifException)
{
	try
	{
	cout << "Début de traitement etatCivil" << endl;
	traitementAge(age);
	cout << "Fin de traitement etatCivil" << endl;
	}
	catch (AgeNegatifException e) {
		cout << "traitement partiel de l'exception" << endl;
		throw 'c'; // Relance d'exception
	}
}


void traitementAge(int age) throw(AgeNegatifException)
{
	cout << "Début de traitement age" << endl;
	if (age <= 0)
	{
		throw AgeNegatifException(age);
	}
	cout << "fin de traitement" << endl;
}