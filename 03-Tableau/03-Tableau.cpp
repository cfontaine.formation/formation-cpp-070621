
#include <iostream>
using namespace std;

int main()
{
	// Tableau à une dimension
	// Déclaration: type nom[taille]
	int tab[5];

	// Initialisation du tableau
	for (int i = 0; i < 5; i++) {
		tab[i] = 0;
	}

	// Accès à un élément du tableau
	tab[1] = 3;
	cout << "tab[1]=" << tab[1] << endl;

	// Déclaration et initialisation d'un tableau
	char tabChr[] = { 'a','z','e' };

	// Parcourir un tableau
	for (int i = 0; i < 3 < i; i++)
	{
		cout << tabChr[i] << endl;
	}

	// Parcourir un tableau en C++11
	// Pas de modification des éléments du tableau
	for (char c : tabChr)
	{
		cout << c << endl;
		c = '1';
	}

	//  En utilisant une référence, on peut  modifier des éléments du tableau
	for (char& c : tabChr)
	{
		cout << c << endl;
		c = '1';
	}

	// Calculer la taille d'un tableau ( /!\ ne fonctionne pas avec les tableaux passés en paramètre de fonction)
	cout << "nombre d'octet du tableau=" << sizeof(tab) << "nb element" << sizeof(tab) / sizeof(tab[0]) << endl;

	// Exercice : tableau
	// Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3
	int t[] = { -7, 4, 8, 0, -3 };
	int max = t[0];
	double somme = 0.0;
	for (int i = 0; i < 5; i++)
	{
		if (t[i] > max)
		{
			max = t[i];
		}
		somme += t[i];
	}
	cout << "maximum=" << max << " moyenne=" << somme / 5 << endl;

	// Tableau à 2 dimensions
	// Déclaration
	double tab2D[3][4];

	// Initialisation du tableau
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			tab2D[i][j] = 0.0;
		}
	}

	// Accès à un élément
	tab2D[0][2] = 4.5;

	// Calculer le nombre  de colonnne  d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre de fonction)
	int nbColonne = (sizeof(tab2D[0]) / sizeof(tab2D[0][0]));

	// Calculer le nombre  de ligne  d'un tableau
	int nbLigne = (sizeof(tab2D) / sizeof(tab2D[0]));

	cout << nbLigne << " " << nbColonne << endl;

	// Déclaration et initialisation
	int tab2di[3][2] = { {1,3},{4,5},{3,7} };

	// Parcourir le tableau
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << tab2D[i][j] << "\t";
		}
		cout << endl;
	}

	// Pointeur
	double val = 5.0;

	// Déclaration d'un pointeur de double
	double* ptr;

	// &val => adresse de la variable val
	cout << &val << endl;
	ptr = &val;	// affectation du pointeur avec l'adresse de la variable
	cout << ptr << endl;

	// *ptr => Accès au contenu de la variable pointer par ptr
	cout << *ptr << endl;
	*ptr = 7.0;
	cout << val << endl;

	// Pointeur de pointeur de double
	double** ptrptr = &ptr;
	cout << *ptrptr << endl;

	// Un pointeur qui ne pointe sur rien
	double* ptr2 = 0; //  0 en c++98  (correspond à NULL en C)
	double* ptrCpp11 = nullptr; // nullptr en C++11

	//  On peut affecter un pointeur avec un pointeur du même type
	ptr2 = ptr;	// ptr2 contient aussi l'adresse de val
	cout << *ptr2 << endl;

	// On peut comparer 2 pointeurs du même type ou compare un pointeur à 0 ou nullptr
	if (ptr2 == ptrCpp11) {
		cout << "pointeurs égaux" << endl;
	}

	if (ptrCpp11 == 0) {	// ou ptr3==nullptr en C++ 11
		cout << "pointeur null" << endl;
	}

	// Pointeur constant
	// En préfixant un pointeur avec const => Le contenu de la variable pointée est constant
	int valInt = 35;
	const int* ptrCst = &valInt;
	cout << *ptrCst << endl;
	// *ptrCst = 20;	// on ne peut plus modifier le contenu pointé par l'intermédiaire du pointeur
	ptrCst = 0;			// Mais on peut modifier le pointeur

	// En postfixant un pointeur avec const => le pointeur est constant
	int* const ptrCstPost = &valInt;
	cout << *ptrCstPost << endl;
	*ptrCstPost = 3;
	cout << val << endl;
	//ptsCstPost = 0;	 // Le pointeur ptrCstPost  est constant, on ne peut plus le modifier

	 // En préfixant et en postfixant un pointeur avec const: le pointeur ptrCst2 est constant et
	// l'on ne peut plus modifier le contenu de la variable pointée par l'intermédiaire du pointeur
	const int* const ptrCst2 = &valInt;
	cout << *ptrCst2 << endl;
	//*ptrCst2 = 56;
	//*ptrCst2 = 0;

	// Opérateur const_cast
	ptrCst = &valInt;
	int* ptrCst3 = (int*)ptrCst; // en C

	ptrCst3 = const_cast<int*>(ptrCst); // en C++, const_cast permet de supprimer le qualificatif const
	*ptrCst3 = 700;
	cout << valInt << endl;

	// Exercice Pointeur
	int v1 = 10, v2 = 20, v3 = 30;
	int* ptrChoix = 0;
	int choix;
	cin >> choix;
	switch (choix)
	{
	case 1:
		ptrChoix = &v1;
		break;
	case 2:
		ptrChoix = &v2;
		break;
	case 3:
		ptrChoix = &v3;
		break;
	default:
		cout << "erreur Choix " << endl;
	}
	if (ptrChoix != 0) {
		cout << *ptrChoix << endl;
	}

	// Tableau et Pointeur
	int tab3[] = { 1,2,3,8,4 };
	int* ptrTab = tab3;			// Le nom d'un tableau est un pointeur sur le premère élément
	cout << *ptrTab << endl;	// On accède au premier élément du tableau

	// On accède au deuxième élément du tableau
	cout << *(ptrTab + 1) << endl;	// équivalent à ptrTab + sizeof(int) ou à tab3[1]
	cout << ptrTab[1] << endl;		// on peut accèder à un tableau avec un pointeur comme à un tableau statique avec l'opérateur []

	ptrTab++;
	cout << *ptrTab << endl; // équivalent à tab3[1]

	// poiteur void => C
	void* ptrVoid = ptr;
	double* ptrd2 = (double*)ptrVoid;

	// reinterpret_cast
	int str = 0x61 | 0x64 << 8; // 00 00 64 61
	int* ptrStr = &str;
	cout << hex << *ptrStr << endl;
	char* ptrC = reinterpret_cast<char*>(ptrStr);
	cout << *ptrC << endl;		// 61 => a
	cout << *(ptrC + 1) << endl;	// 64 => d

	// Exercice Tableau et Pointeur
	int tp[] = { 2,5,8,9,23 };
	int choix2 = 0;
	cin >> choix2;
	if (choix2 > 0 && choix2 <= 5) {
		int* ptrChoix2 = tp + choix2 - 1; // &tp[choix2-1]
		cout << *ptrChoix2 << endl;
	}
	else
	{
		cout << "erreur" << endl;
	}

	// Allocation de mémoire dynamique
	int* ptrInt = new int;	// new => allocation de la mémoire
	*ptrInt = 45;
	cout << *ptrInt << " " << ptrInt << endl;
	delete ptrInt;			// delete => libération de la mémoire
	cout << ptrInt << endl;
	ptrInt = 0;

	// Allocation dynamique avec initialisation
	ptrInt = new int(45);	// en C++ 98
	cout << *ptrInt << endl;
	delete ptrInt;

	ptrInt = new int{ 45 }; // en C++11
	cout << *ptrInt << endl;
	delete ptrInt;

	// Allocation dynamique d'un tableau
	int size = 5;
	char* ptrChrDyn = new char[size];	// new [] => création d'un tableau dynamique
	*ptrChrDyn = 'A';			// ptrChrDyn[0]='A';
	*(ptrChrDyn + 1) = 'B';		// ptrChrDyn[1]='B';
	// On peut accèder à un tableau dynamique comme à un tableau statique
	cout << ptrChrDyn[0] << endl;// A
	cout << ptrChrDyn[1] << endl;// B
	ptrChrDyn[2] = 'C';
	delete[] ptrChrDyn;	 // delete [] => libération d'un tableau dynamique

	// Exercice : Tableau dynamique
	// Modifier le programme Tableau pour faire la saisie :
	// - de la taille du tableau
	// - des éléments du tableau
	// Trouver la valeur maximale et la moyenne du tableau
	cout << "Entrer la taille du tableau" << endl;
	int size;
	cin >> size;
	int* td = new int[size];
	for (int i = 0; i < size; i++)
	{
		cout << "t[" << i << "]=";
		cin >> td[i];
	}
	int maximum = td[0];
	double somme = td[0];
	for (int i = 1; i < size; i++)
	{
		if (td[i] > maximum)
		{
			maximum = td[i];
		}
		somme += td[i];
	}
	cout << "maximum=" << maximum << " moyenne=" << somme / size << endl;
	delete[] td;

	// Référence 
	// Une référence correspond à un autre nom que l'on donne à la variable
	// on doit aussi intialiser une référence avec une lvalue (= qui possède une adresse)
	int vr = 45;
	int& ref = vr;
	cout << ref << endl;
	ref = 3;
	cout << vr << endl;

	//int& ref2 = 45;		// Erreur pas d'initialisation avec une littéralle

	// Référence constante
	const int& refCst = vr;
	cout << refCst << endl;
	//refCst = 345;		 // On ne peut plus utiliser une référence constante pour modifier la valeu

	const int& refCst2 = 123;	// On peut initialiser une référence constante avec une valeur litérale
	cout << refCst2 << endl;
}