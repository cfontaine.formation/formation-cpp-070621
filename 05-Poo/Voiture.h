#pragma once
#include<string>
#include <iostream>
#include "Personne.h"
#include "Moteur.h"
class Voiture
{
	// Variable d'instance
	std::string marque;
	std::string couleur;
protected:
	std::string plaqueIma;
private:
	int vitesse;

	// Agr�gation 
	Personne* proprietaire;

	Moteur *moteur;
	//int* data;

	// variable de classe
	static int compteurVoiture;

public:
	static const int VITESSE_MAX = 200;

	// M�thodes d'intances
	void accelerer(int diffVitesse);
	void freiner(int diffVitesse);
	void arreter();
	bool estArreter() const;
	virtual void afficher() const;

	//Constructeur
	Voiture() : marque("Opel"), couleur("noir"), plaqueIma("WW-WWWW-WW"), vitesse(0),proprietaire(0)
	{
		std::cout << "Constructeur par d�faut Voiture" << std::endl;
		moteur = new Moteur(10);
		compteurVoiture++;
		//data = new int[6];
	}

	Voiture(std::string marque, std::string couleur, std::string plaqueIma) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), proprietaire(0)
	{
		compteurVoiture++;
		moteur = new Moteur(10);
		//data = new int[6];
	}

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, int vitesse) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(vitesse), proprietaire(0)
	{
		compteurVoiture++;
		moteur = new Moteur(10);
		//data = new int[6];
	}

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, int vitesse,Personne *proprietaire) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(vitesse), proprietaire(proprietaire)
	{
		compteurVoiture++;
		moteur = new Moteur(10);
		//data = new int[6];
	}

	Voiture(const Voiture& v);

	~Voiture();

	static void testMethodeClasse();
	static bool egaliteVitesse(const Voiture& v1, const Voiture& v2);
	//getter =>inline
	int getVitesse() const // const quand il n'ya pas de modification des variables d'instance
	{
		return vitesse;
	}

	void setVitesse(int vitesse) 
	{
		if (vitesse >= 0)
		{
			this->vitesse = vitesse;
		}
	}

	std::string getMarque() const
	{
		return marque;
	}

	std::string getCouleur() const
	{
		return couleur;
	}

	void setCouleur(std::string  couleur)
	{
		this->couleur = couleur;
	}

	std::string getPlaqueIma() const
	{
		return plaqueIma;
	}

	void setPlaqueIma(std::string plaqueIma)
	{
		this->plaqueIma = plaqueIma;
	}

	Personne* getProprietaire() const
	{
		return proprietaire;
	}

	void setProprietaire(Personne* proprietaire)
	{
		this->proprietaire = proprietaire;
	}

	static int getCompteurVoiture() // pas const sur les m�thodes de classe
	{
		return compteurVoiture;
	}
};

