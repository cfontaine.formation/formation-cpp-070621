#include <iostream>
#include "Voiture.h"
#include "CompteBancaire.h"
#include "VoiturePrioritaire.h"
using namespace std;

int main()
{
	// Accès variable de classe
	cout << "Compteur Voiture="<<Voiture::getCompteurVoiture() << endl;
	cout << "vitesse Max=" << Voiture::VITESSE_MAX << endl;
	//Appel méthode de classe
	Voiture::testMethodeClasse();

	// Statique
	Voiture v1;
	cout << "Compteur Voiture=" << Voiture::getCompteurVoiture() << endl;
	v1.setVitesse(10);//v1.vitesse = 10;
	cout << v1.getVitesse() << endl; //v1.vitesse
	v1.accelerer(30);
	v1.afficher();
	v1.freiner(20);
	v1.afficher();
	cout << v1.estArreter() << endl;
	v1.arreter();
	v1.afficher();
	cout << v1.estArreter() << endl;

	// Dynamique
	Voiture* v2 = new Voiture("Honda", "Blanc", "FR-5600-AD");
	cout << "Compteur Voiture=" << Voiture::getCompteurVoiture() << endl; //Voiture::compteurVoiture 
	v2->afficher();
	v2->setVitesse(20);// v2->vitesse = 20;
	cout << v2->getVitesse() << endl; // v2->vitesse
	delete v2;

	Voiture v3("Fiat", "jaune", "fr-5934-RT");
	cout << "Compteur Voiture=" << Voiture::getCompteurVoiture() << endl;
	v3.afficher();

	// appel du constructeur par copie
	Voiture v4 = v3;
	cout << "Compteur Voiture=" << Voiture::getCompteurVoiture() << endl;
	// Voiture v4(v3);
	
	cout<<Voiture::egaliteVitesse(v3, v4)<<endl;
	Personne per1("John", "Doe", 46);
	v4.setProprietaire(&per1);
	v4.afficher();
	// Objet constant
	const Voiture v5("Toyota", "Rouge", "az-3456-RT");
	v5.afficher();

	Voiture v6("Renault", "vert", "QS-3466-TY", 0, &per1);
	v6.afficher();


	VoiturePrioritaire vp1;
	vp1.gyroOn();
	vp1.afficher();

	VoiturePrioritaire vp2(true, "BMW", "gris", "AZ-34566UI");
	vp2.afficher();

	Voiture* vp3 = new VoiturePrioritaire(false, "Fiat", "Jaune", "AZ-34566-UI");
	vp3->afficher();
	
	// Compte Bancaire
	CompteBancaire cb("John Doe",100.0);
	//cb.solde = 100.0;
	//cb.iban = "fr5962-0000-0000";
	//cb.titulaire = "John Doe";
	cout << cb.getSolde()<< endl;
	cb.afficher();
	cb.debiter(50.0);
	cb.afficher();
	cb.crediter(100.0);
	cb.afficher();
	cout << cb.estPositif() << endl;

	CompteBancaire cb2("Jane Doe", 500.0);
	cb2.afficher();

}
