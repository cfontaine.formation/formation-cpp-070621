#include "Voiture.h"
#include <iostream>

using namespace std;

// initialisation de la variable de classe
int Voiture::compteurVoiture = 0;

void Voiture::accelerer(int diffVitesse)
{
	if (diffVitesse > 0) {
		vitesse += diffVitesse;
	}
}

void Voiture::freiner(int diffVitesse)
{
	if (diffVitesse > 0) {
		vitesse -= diffVitesse;
	}
	if (vitesse < 0) {
		vitesse = 0;
	}
}

void Voiture::arreter()
{
	vitesse = 0;
}

bool Voiture::estArreter() const
{
	return vitesse == 0;
}

void Voiture::afficher() const
{
	cout << marque << " " << couleur << " " << plaqueIma << " " << vitesse << endl;
	if (proprietaire != 0) {
		proprietaire->afficher();
	}
	moteur->afficher();
}

Voiture::Voiture(const Voiture& v)
{
	cout << "constructeur par copie";
	marque = v.marque;
	couleur = v.couleur;
	plaqueIma = v.plaqueIma;
	vitesse = v.vitesse;
	moteur = new Moteur(10);
	proprietaire = v.proprietaire;
	compteurVoiture++;
}

void Voiture::testMethodeClasse()
{
	cout << "Methode de classe" << endl;
	cout << compteurVoiture << endl;
	//vitesse = 10; // instance pas d'acc�s 
	//afficher();
}

bool Voiture::egaliteVitesse(const Voiture& va, const Voiture& vb)
{
	return va.vitesse == vb.vitesse;
}

Voiture::~Voiture()
{
	cout << "Destructeur voiture" << endl;
	delete moteur;
	//delete[] data;
}

//Voiture::Voiture()
//{
//	marque = "Opel";
//	couleur = "noir";
//	plaqueIma = "WW-WWW-WW";
//	vitesse = 0;
//}

//Voiture::Voiture(std::string marque, std::string couleur, std::string plaqueIma)
//{
//	this->marque = marque;
//	this->couleur = couleur;
//	this->plaqueIma = plaqueIma;
//	vitesse = 0;
//}
