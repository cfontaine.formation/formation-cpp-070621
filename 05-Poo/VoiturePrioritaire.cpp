#include "VoiturePrioritaire.h"
#include <iostream>

using namespace std;

void VoiturePrioritaire::gyroOn()
{
	gyro = true;
	plaqueIma = "a";
}

void VoiturePrioritaire::gyroOff()
{
	gyro = false;
}

void VoiturePrioritaire::afficher() const
{
	Voiture::afficher();
	cout << gyro << endl;
}
