#pragma once
#include "Voiture.h"
#include <iostream>
class VoiturePrioritaire : public Voiture
{
	bool gyro;



public:
	VoiturePrioritaire(): gyro(false) // implicitement il appel le constructeur par d�faut de la clase m�re
	{
		std::cout << "COnstructeur par d�faut VoiturePrioritaire" << std::endl;
	}

	VoiturePrioritaire(bool gyro, std::string marque, std::string couleur, std::string plaqueIma) : Voiture(marque, couleur, plaqueIma), gyro(gyro)
	{

	}

	void gyroOn();

	void gyroOff();

	bool getGyro()const
	{
		return gyro;
	}

	//redefinition
	void afficher() const;
};

