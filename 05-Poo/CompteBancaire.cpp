#include "CompteBancaire.h"
#include <iostream>

using namespace std;

int CompteBancaire::cptCompte = 0;

void CompteBancaire::afficher() const
{
	cout << "_______________" << endl;
	cout << "solde= " << solde << endl;
	cout << "iban= " << iban << endl;
	cout << "titulaire= " << titulaire << endl;
	cout << "_______________" << endl;
}

void CompteBancaire::crediter(int valeurDepot)
{
	if (valeurDepot > 0)
	{
		solde += valeurDepot;
	}
}

void CompteBancaire::debiter(int valeurDebit)
{
	if (valeurDebit > 0)
	{
		solde -= valeurDebit;
	}
}

bool CompteBancaire::estPositif() const
{
	return solde>=0.0;
}
