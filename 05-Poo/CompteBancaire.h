#pragma once
#include <string>

class CompteBancaire
{

	double solde;
	std::string iban;
	std::string titulaire;

	static int cptCompte;
public:
	CompteBancaire(std::string titulaire) : titulaire(titulaire), solde(0), iban("")
	{
		cptCompte++;
		iban = "fr-5962-0000-" + std::to_string(cptCompte);
	}

	CompteBancaire(std::string titulaire, double solde) : titulaire(titulaire), solde(solde), iban("")
	{
		cptCompte++;
		iban = "fr-5962-0000-" + std::to_string(cptCompte);
	}

	void afficher() const;

	void crediter(int valeurDepot);

	void debiter(int valeurDebit);

	bool estPositif() const ;

	double getSolde()const 
	{
		return solde;
	}

	std::string getIban() const
	{
		return iban;
	}

	void setTitulaire(std::string titulaire)
	{
		this->titulaire = titulaire;
	}

};

